const cacheName = "PWANewsAPI-v1";

const assets = [
  "./",
  "./index.html",
  "./main.js",
  "./manifest.json",
  "./news-article.js",
  "./newsapi.js",
  "./styles.css"
];

self.addEventListener("install", e => {
  console.log("service worker installed");

  e.waitUntil(
    caches.open(cacheName).then(cache => {
      console.log("caching shell assets");
      cache.addAll(assets);
    })
  );

});

self.addEventListener("activate", e => {
  console.log("service worker activated");

  e.waitUntil(
    caches.keys().then(keys => {
      return Promise.all(
        keys.filter(key => key !== cacheName)
          .map(key => caches.delete(key))
      );
    })
  );
});

self.addEventListener('fetch', e => {
// self.addEventListener('fetch', async e => {
  const req = e.request;
  const url = new URL(req.url);

  if (url.origin === location.origin) {
    e.respondWith(cacheFirst(req));
  } else {
    e.respondWith(networkAndCache(req));
  }
});

async function cacheFirst(req) {
  const cache = await caches.open(cacheName);
  const cached = await cache.match(req.url);
  return cached || fetch(req);
}

// function cacheFirst(req) {
//   return caches.open(cacheName)
//     .then(cache => return cache.match(req.url))
//     .then(cacheRes => return cacheRes || fetch(req));
// }

async function networkAndCache(req) {
  const cache = await caches.open(cacheName);
  try {
    const fresh = await fetch(req);
    await cache.put(req, fresh.clone());
    return fresh;
  } catch (e) {
    const cached = await cache.match(req.url);
    return cached;
  }
}

// function networkAndCache(req) {
//   return caches.open(cacheName)
//     .then(cache => {
//       return fetch(req)
//         .then(fresh => {
//           cache.put(req, fresh.clone());
//           return fresh;
//         })
//         .catch(err => {
//           return cache.match(req.url)
//         });
//     });
// }
