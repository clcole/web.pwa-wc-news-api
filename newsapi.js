const apiKey = 'c1fc89bac7a64bdea2e2a2c02d67fe87';

const topHeadlinesUrl =
  'https://newsapi.org/v2/top-headlines?country=us&pageSize=8&apiKey=' + apiKey;

export { apiKey, topHeadlinesUrl };