class NewsArticle extends HTMLElement {

  constructor(){
    super();
    this.root = this.attachShadow({mode: "open"});
  }

  set article(article) {
    // shadow dom shields component from any external css
    this.root.innerHTML = `
      <style>
        a {
          text-decoration: none;
          color: #333;
        }
        
        article {
          width: 100%;
          height: 100%;
          box-shadow: 0 3px 5px 0 rgba(0, 0, 0, 0.3);
        }
        
        article img {
          width: 100%;
          height: auto;
        }
        
        .title {
          padding: 15px;
          text-align: center;
        }
      </style>

      <article>
        <a href="${article.url}">
          <img src="${article.urlToImage || ''}">
          <div class="title">${article.title}</div>
        </a>
      </article>
    `;
  }

}

customElements.define("news-article", NewsArticle);
