import { topHeadlinesUrl } from "./newsapi.js";
import "./news-article.js";

window.addEventListener("DOMContentLoaded", () => {
  registerSW();
  fetchNews();
});

function registerSW() {
  if ("serviceWorker" in navigator) {
    navigator.serviceWorker.register("./sw.js")
      .then((reg) => console.log("service worker registered", reg))
      .catch((err) => console.log("service worker failed to register", err));
  }
}

async function fetchNews() {
  const main = document.querySelector("main");
  let fragment = new DocumentFragment();

  try {
    const res = await fetch(topHeadlinesUrl);
    const json = await res.json();

    json.articles.forEach(article => {
      const el = document.createElement("news-article");
      el.article = article;
      fragment.appendChild(el);
    });
  }
  catch {
    const el = document.createElement("news-article");
    el.innerHTML = `<div>Unable to fetch news</div>`;
    fragment.appendChild(el);
  }

  main.appendChild(fragment);
}

